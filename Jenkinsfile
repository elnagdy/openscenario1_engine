/********************************************************************************
 * Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

pipeline {
  agent none
  options {
    checkoutToSubdirectory('repo')
    timeout(time: 2, unit: 'HOURS')
  }
  stages {
    stage('Linux and Windows build') {
      parallel {
        stage('Linux') {
          agent {
            kubernetes {
              label 'openscenarioengine_image-agent-pod-' + env.BUILD_NUMBER
              yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: openscenarioengine-image
    image: nmraghu/openscenarioengine_image:v0.0.3
    tty: true
    resources:
      limits:
        memory: "16Gi"
        cpu: "4"
      requests:
        memory: "16Gi"
        cpu: "4"
  - name: jnlp
    volumeMounts:
    - name: volume-known-hosts
      mountPath: /home/jenkins/.ssh
  volumes:
  - name: volume-known-hosts
    configMap:
      name: known-hosts
"""
            }
          }
          environment {
            CONAN_USER_HOME = '/OSE/conan'
          }
          stages {
            stage('Linux: Prepare Dependencies') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/10_build_prepare.sh'
                    sh 'bash repo/utils/ci/scripts/15_prepare_thirdParty.sh'
                }
              }
            }
            stage('Linux: Configure build') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/20_configure.sh'
                }
              }
            }

            stage('Linux: Build and install project') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/30_build.sh'
                }
              }
            }
            stage('Linux: Build and run unit tests') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/40_unit_tests.sh'
                }
              }
            }
          }
        }
        stage('Windows') {
          agent {
            label 'windows'
          }
          environment {
            MSYSTEM = 'MINGW64'
            CHERE_INVOKING = 'yes'
            PYTHON_WINDOWS_EXE = 'C:/Program Files/Python39/python.exe'
          }
          stages {
            stage('Windows: Prepare dependencies') {
              steps {
                bat 'subst W: %WORKSPACE%'
                dir('W:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/10_build_prepare.sh'
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/15_prepare_thirdParty.sh'
                }
              }
            }
            stage('Windows: Configure build') {
              steps {
                dir('W:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/20_configure.sh'
                }
              }
            }
            stage('Windows: Build and install project') {
              steps {
                dir('W:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/30_build.sh'
                }
              }
            }
            stage('Windows: Build and run unit tests') {
              steps {
                dir('W:/') {
                  bat 'C:\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/40_unit_tests.sh'
                }
              }
            }
          }
          post {
            always {
              bat 'subst W: /d'
            }
          }
        }
      }
    }
  }
}
