/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"

TEST(RelativeDistanceTypeTest, GivenUnkownCoorodinates)
{
  auto relativeDistanceType = OpenScenarioEngine::v1_2::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::UNKNOWN);

  ASSERT_EQ(OpenScenarioEngine::v1_2::RelativeDistanceType::kUnknown, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenEntityCoorodinates)
{
  auto relativeDistanceType = OpenScenarioEngine::v1_2::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::CARTESIAN_DISTANCE);

  ASSERT_EQ(OpenScenarioEngine::v1_2::RelativeDistanceType::kCartesian_distance, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenLaneCoorodinates)
{
  auto relativeDistanceType = OpenScenarioEngine::v1_2::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::EUCLIDIAN_DISTANCE);

  ASSERT_EQ(OpenScenarioEngine::v1_2::RelativeDistanceType::kEuclidian_distance, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenRoadCoorodinates)
{
  auto relativeDistanceType = OpenScenarioEngine::v1_2::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::LATERAL);

  ASSERT_EQ(OpenScenarioEngine::v1_2::RelativeDistanceType::kLateral, relativeDistanceType);
}

TEST(RelativeDistanceTypeTest, GivenTrajectoryCoorodinates)
{
  auto relativeDistanceType = OpenScenarioEngine::v1_2::ConvertScenarioRelativeDistanceType(NET_ASAM_OPENSCENARIO::v1_2::RelativeDistanceType::LONGITUDINAL);

  ASSERT_EQ(OpenScenarioEngine::v1_2::RelativeDistanceType::kLongitudinal, relativeDistanceType);
}