################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building units with Conan
################################################################################

import os
from conan.tools.files import rm
from conans import tools, CMake, ConanFile
from conan.tools.files import patch, export_conandata_patches

required_conan_version = ">=1.47.0"

class UnitsConan(ConanFile):
    name = "units"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "Units_Tests": [True, False],
               "cmake_generator": ["MSYS Makefiles", "Unix Makefiles", "Visual Studio", "Ninja"]}
    default_options = {"shared": False,
                       "fPIC": True,
                       "Units_Tests": False}
    exports_sources = "*"
    no_copy_source = False
    short_paths = True
    generators = "cmake"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get(
                    "CONAN_CMAKE_GENERATOR", "MSYS Makefiles")
        else:
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get(
                    "CONAN_CMAKE_GENERATOR", "Unix Makefiles")

    def export_sources(self):
        export_conandata_patches(self)

    def _patch_sources(self):
        os.chdir(self.name)
        os.system('git apply ../patches/pascal-name-conflict.patch')

    def _get_url_sha(self):
        if "CommitID" in self.version:
            url = self.conan_data["sources"][self.version.split("_")[0]]["url"]
            sha256 = self.version.split("_")[1]
        else:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = tools.Git(folder=self.name)
        git.clone(url)
        git.checkout(sha256)
        self._patch_sources()

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_GENERATOR"] = self.options.cmake_generator
        cmake.definitions["BUILD_TESTS"] = self.options.Units_Tests
        cmake.configure(source_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)
