################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building open scenario engine with Conan
################################################################################

from conan.errors import ConanInvalidConfiguration
from conan.tools.files import rm 
from conans import tools, CMake, ConanFile
import os

required_conan_version = ">=1.47.0"

class OpenScenarioEngineConan(ConanFile):
    name = "openscenario_engine"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], 
               "fPIC": [True, False],
               "cmake_generator": ["MSYS Makefiles", "Unix Makefiles", "Visual Studio", "Ninja"],
               "MantleAPI_version":["ANY"],
               "openscenario_api_version":["ANY"],
               "units_version":["ANY"],
               "Yase_version":["ANY"]
               }

    default_options = {"shared": False, 
                       "fPIC": True,
                       "openscenario_api_version":"v1.3.1",
                       "units_version":"2.3.3"}

    no_copy_source = False
    short_paths = True
    generators = "cmake"

    def requirements(self):
        self.requires(f"openscenario_api/{self.options.openscenario_api_version}@openpass/testing")
        self.requires(f"units/{self.options.units_version}@openpass/testing")
        self.requires(f"Yase/{self.options.Yase_version}@openpass/testing")
        self.requires(f"MantleAPI/{self.options.MantleAPI_version}@openpass/testing")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get("CONAN_CMAKE_GENERATOR", "MSYS Makefiles")
        else:
            if not self.options.cmake_generator:
                self.options.cmake_generator = self.env.get("CONAN_CMAKE_GENERATOR", "Unix Makefiles")

    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version
        
        return url, sha256
        
    def source(self): 
        url, sha256 = self._get_url_sha()
        git = tools.Git()
        git.clone(url, "main")
        git.checkout(sha256)

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_BUILD_TYPE"] = "Release"
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = "bin"
        cmake.definitions["CMAKE_PREFIX_PATH"] = ";".join(self.deps_cpp_info.rootpaths + self.deps_cpp_info.libdirs)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*", src="bin")
        self.copy("*", src=os.path.join(self.source_folder, "engine/cmake"), dst="lib/cmake/OpenScenarioEngine/deps")
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)

    def package_info(self):
        self.cpp_info.libs = ["openscenario_engine"]
